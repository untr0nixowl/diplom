﻿using System;

namespace MyDiplomaDTO
{
    public class MedicalCard
    {
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }
        public string Comments { get; set; }
    }
}