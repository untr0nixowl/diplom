﻿namespace MyDiplomaDTO
{
    public class Address
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int HouseNo { get; set; }
        public int? CorpusNo { get; set; }
        public int Room { get; set; }

    }
}