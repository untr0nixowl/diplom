﻿namespace MyDiplomaDTO
{
    public class Status
    {
        public int Id { get; set; }
        public string StatusType { get; set; }
        public string Comments { get; set; }
    }
}