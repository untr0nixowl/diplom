﻿namespace MyDiplomaDTO
{
    public class ConnectionType
    {
        public int Id { get; set; }
        public string Connection { get; set; }
        public string Comments { get; set; }
    }
}