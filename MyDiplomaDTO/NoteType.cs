﻿namespace MyDiplomaDTO
{
    public class NoteType
    {
        public int Id { get; set; }
        public string NoteTypeString { get; set; }
        public string Comments { get; set; }
    }
}