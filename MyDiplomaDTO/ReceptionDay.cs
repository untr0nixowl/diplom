﻿using System;

namespace MyDiplomaDTO
{
    public class ReceptionDay
    {
        public int Id { get; set; }
        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}