﻿namespace MyDiploma.Data
{
    public class ReceptionDay : MyDiplomaDTO.ReceptionDay
    {

        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }

    }
}