﻿using MyDiplomaDTO;
using System;

namespace MyDiploma.Data
{
    public class Doctor : User
    {
        public int Rating { get; set; }
        public IObservable<ReceptionDay> ReceptionDays { get; set; }
        public IObservable<MedicalCard> Cards { get; set; }
        public IObservable<Meeting> Meetings { get; set; }
        public IObservable<Comment> Comments { get; set; }
    }
}