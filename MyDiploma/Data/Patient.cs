﻿using MyDiplomaDTO;
using System;

namespace MyDiploma.Data
{
    public class Patient : User
    {
        public Address Address { get; set; }
        public MedicalCard MedicalCard { get; set; }
        public IObservable<Meeting> Meetings { get; set; }
        public IObservable<Comment> Comments { get; set; }


    }
}