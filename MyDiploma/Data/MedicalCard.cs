﻿using System;

namespace MyDiploma.Data
{
    public class MedicalCard : MyDiplomaDTO.MedicalCard
    {
        public IObservable<MedicalNote> Notes { get; set; }

    }
}