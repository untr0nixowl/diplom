﻿using MyDiplomaDTO;

namespace MyDiploma.Data
{
    public class Meeting : MyDiplomaDTO.Meeting
    {
        public int PatientId { get; set; }
        public Patient Patient { get; set; }
        public Status Status { get; set; }
        public int ReceptionDayId { get; set; }
        public ReceptionDay ReceptionDay { get; set; }
    }
}