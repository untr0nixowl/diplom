﻿namespace MyDiploma.Data
{
    public class Comment
    {
        public int Id { get; set; }
        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }
        public string CommentString { get; set; }
        public int Rating { get; set; }
    }
}