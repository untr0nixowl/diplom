﻿using MyDiplomaDTO;

namespace MyDiploma.Data
{
    public class MedicalNote
    {
        public int Id { get; set; }
        public int MedicalCardId { get; set; }
        public MedicalCard MedicalCard { get; set; }
        public NoteType Type { get; set; }
        public int DoctorId { get; set; }
        public Doctor Doctor { get; set; }
        public string Note { get; set; }
    }
}